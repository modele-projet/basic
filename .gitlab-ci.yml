default:
  image: alpine:latest

# CI Variables : disabled various job when set to a non-empty string
variables:
  AUTOBUMP_DISABLED: ""  # Disable autobump on commit to default branch
  CODE_QUALITY_DISABLED: ""  # Disable all code quality related jobs
  SAST_DISABLED: ""  # Disable all sast related jobs
  SECRET_DETECTION_DISABLED: ""  # Disable gitlab secret detection job
  MR_TITLE_CHECK_DISABLED: ""  # Disable checking MR title to be a proper commit message

.external_trigger_rules: &external_trigger_rules
  - if: $CI_PIPELINE_SOURCE == "web"  # triggered "Run Pipeline" button in CI/CD -> Pipelines
  - if: $CI_PIPELINE_SOURCE == "api"  # triggered by the pipelines API
  - if: $CI_PIPELINE_SOURCE == "chat" ## triggered by gitlab ChatOps
  - if: $CI_PIPELINE_SOURCE == "trigger"  # triggered by a trigger token
  - if: $CI_PIPELINE_SOURCE == "webide"  # triggered from gitlab web IDE
  - if: $CI_PIPELINE_SOURCE == "schedule"  # triggered by schedule pipelines

.git_submodules_install_on_alpine: &git_submodules_install_on_alpine
    - apk update
    - apk add make
    - apk add git
    - make submodule-local-update

# workflow defines when the whole pipeline is run (and precise jobs are picked with `rules`)
# This workflow means : on merge request event (creation or push), on any outside 
# trigger (web, api, chat,...) and on push to any branch
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - *external_trigger_rules
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

stages:
  - .pre
  - security
  - test
  - quality
  - auto-bump

# Include Code quality and security checks from gitlab templates
include:
  - template: Jobs/Code-Quality.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml

#####################################
# Security jobs                     #
# Run on push outside merge request #
# May NOT fail                      #
#####################################
.sast_rules: &sast_rules
  - if: $SAST_DISABLED
    when: never
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  - *external_trigger_rules
  - if: $CI_COMMIT_BRANCH

# Modify behavior of gitlab template
sast:
  stage: security
  allow_failure: false
  needs: []

# Modify behavior in gitlab template
.secret-analyzer:
  stage: security
  allow_failure: false
  needs: []

# Modify behavior in gitlab template
secret_detection:
  rules:
    - *sast_rules
#################################################################################################
# Jobs that run on each pipeline (manually for push on non default branch and on merge request) #
#################################################################################################
test-init:
  stage: test
  before_script:
    - apk add git
    - apk add jq
    - apk add openssh-keygen
  script:
    - cp -f ./tests/config_test ./config
    - ./init.sh -n
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
      allow_failure: true
    - *external_trigger_rules
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
    - if: $CI_COMMIT_BRANCH
      when: manual
      allow_failure: true
##################################
# Quality checks jobs :          #
# Run on demand in merge request #
# May fail                       #
##################################
.quality_checks_rules: &quality_checks_rules
  - if: $CODE_QUALITY_DISABLED
    when: never
  - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    when: manual
    allow_failure: true
  - *external_trigger_rules

shellcheck:
  image: koalaman/shellcheck-alpine:latest
  stage: quality
  before_script:
    - *git_submodules_install_on_alpine
    - apk add --update make
  script:
    - make test-sh
  needs: []
  rules:
    - *quality_checks_rules

# Modify behavior in gitlab template
code_quality:
  before_script:
    - *git_submodules_install_on_alpine
  stage: quality
  rules:
    - *quality_checks_rules
##################################
# Control of merge request title #
# Run only on merge request      #
##################################
check_merge_request_title:
  stage: .pre
  image: fkbbegue/auto-bump:2.35.0-ubuntu@sha256:6af05baa3ac16da79d130fa06d3b9223523859c6c83a5a7231413d1efd0533db
  script:
    - echo "This job check if the merge request title is a well-formed commit title (with respect to conventional commit)"
    - echo "It is intended for teams using merge request title to name merge/squash commit on default branch"
    - echo "This job might be disabled by setting the MR_TITLE_CHECK_DISABLED variable to any non empty string"
    - cz check --message "$CI_MERGE_REQUEST_TITLE"
  rules:
    - if: $MR_TITLE_CHECK_DISABLED
      when: never
    - if: $CI_MERGE_REQUEST_TITLE =~ /^(\[Draft\]|\(Draft\)|Draft:)/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

######################################
# Auto bump                          #
# Run only on push to default branch #
######################################
bump_version:
  stage: auto-bump
  image: fkbbegue/auto-bump:2.35.0-ubuntu@sha256:6af05baa3ac16da79d130fa06d3b9223523859c6c83a5a7231413d1efd0533db
  script:
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null # add ssh key
    - GIT_SSH_URL="git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git"
    # - ssh-keyscan ${CI_SERVER_HOST} >> ~/.ssh/known_hosts
    - git remote set-url origin $GIT_SSH_URL
    - git config --global user.email "${CI_EMAIL}"
    - git config --global user.name "${CI_USERNAME}"
    - git checkout $CI_COMMIT_REF_NAME
    - git pull --ff-only
    - cz -nr 3,21 bump --yes # execute auto bump (cz bump automatically do something equivalent to git add . && git commit)
    - git push origin HEAD:$CI_COMMIT_REF_NAME
    - TAG="v$(cz version -p)"
    - git push origin $TAG
  rules:
      - if:  $AUTOBUMP_DISABLED
        when: never
      - if: $CI_COMMIT_MESSAGE =~ /\[(no[ -]?bump|bump[ -]skip|skip[ -]bump)]/
        when: never
      - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
        when: on_success