# {{config.project_name}}

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://conventionalcommits.org)
[![img](https://img.shields.io/badge/semver-2.0.0-green)](https://semver.org/)

{{config.project_description}}

## Getting Started

### Requirements

\<Requirements\>

[back to top](#{{config.project_name_anchor}})

### Installation

\<Project-Installation-Steps\>

[back to top](#{{config.project_name_anchor}})

## Changelog

See the CHANGELOG.md file

[back to top](#{{config.project_name_anchor}})

## License

{{config.project_name}}  
Copyright (C) {{config.year}}  {{config.project_authors}}  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

[back to top](#{{config.project_name_anchor}})
