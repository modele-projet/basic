.PHONY: test-sh
test-sh: ## Run shellcheck on every .sh file
	@echo "+ $@"
	find . -type f -name "*.sh" -exec "shellcheck" "-x" {} \;

.PHONY: submodule-remote-update
submodule-remote-update: ## Update submodule to its last version on main
	@echo "+ $@"
	git submodule update --remote 
	git add src/git-submodules/

.PHONY: submodule-local-update
submodule-local-update: ## Update submodule to the required version for this project
	@echo "+ $@"
	git submodule update --init --recursive 