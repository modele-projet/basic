# CHANGELOG

## v0.13.2 (2022-10-25)

### Refactor

- Extract main logic in Installer in order to reuse the code more easily for other template

## v0.13.1 (2022-10-16)

### Fix

- change license also for new projects

## v0.13.0 (2022-10-16)

### Feat

- change license from GPLV3+ to GPLV3

## v0.12.0 (2022-10-10)

### Feat

- add support for ssh host different from gitlab server url

## v0.11.1 (2022-10-10)

### Fix

- fix unstable autobump

## v0.11.0 (2022-10-07)

### Feat

- remove docs from directories ignored by codeclimate

## v0.10.0 (2022-10-07)

### Feat

- add more precise configuration to markdownlint

## v0.9.1 (2022-10-05)

### Fix

- fix an inversion between git username and git email in init.sh

## v0.9.0 (2022-10-05)

### Feat

- Add color to terminal output for better lisibility

## v0.8.0 (2022-09-26)

### Feat

- **Gitlab**: Add creation of a deploy key during initialization of gitlab repository for autobump feature

## v0.7.0 (2022-09-21)

### Feat

- Add customization of new project throught config file or user input

### Fix

- **CI**: Fix a problem with ssh port in CI

## v0.6.1 (2022-09-07)

### Fix

- **CI**: Disable check of MR title on draft MR

## v0.6.0 (2022-08-31)

### Feat

- **ci**: Add a job to check that the mr title is a well-formed commit title

## v0.5.0 (2022-08-31)

### Feat

- **ci**: Add a way to skip auto-bump with a tag in commit message

## v0.4.0 (2022-08-30)

### Feat

- **CI**: Configure code quality check

## v0.3.2 (2022-08-30)

### Fix

- **ci**: set allow failure to false for secret analyser

## v0.3.1 (2022-08-29)

### Fix

- **ci**: allow pipeline to be triggered manually

## v0.3.0 (2022-08-29)

### Feat

- **CI**: automatically bump version after a push on default branch

### Refactor

- **CI**: Change DISABLE_AUTOBUMP to AUTOBUMP_DISABLED to be compliant with gitlab ci

## v0.2.2 (2022-08-27)

### Fix

- **README**: fix missing link in SemVer badge

## v0.2.1 (2022-07-31)

### Fix

- fix misalignment of badges on README.md

## v0.2.0 (2022-07-31)

### Feat

- add a CI support with Gitlab-CI

## v0.1.1 (2022-07-30)

### Fix

- fix top header that appeared in README.md title

## v0.1.0 (2022-07-29)

### Feat

- add default project tree
- add an init.sh file to initialize the repo after cloning
- add commitizen to help enforce SemVer 2.0.0 and Conventional Commit 1.0.0
- add .gitignore file

### Fix

- **git**: add log files to gitignore

## v0.0.0 (2022-07-25)
